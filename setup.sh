#!/bin/bash

set -e
set -u

# IMPORTANT Make sure to increment VERSION_PATCH on every change of this file!

VERSION_MAJOR=1
VERSION_MINOR=3
VERSION_PATCH=6

VERSION="${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}"

log() {
	if [ $opt_quiet = 1 ]; then
		return
	fi
	local -r msg="$1"
	echo "$msg"
}

err() {
	local -r msg="$1"
	>&2 echo "ERROR: $msg"
}

is_in() {
	local -r needle="$1"
	local -r haystack="$2"
	set +e
	echo "$haystack" | grep -q "$needle"
	local -r rc=$?
	set -e
	echo $rc
}

# depends: dpkg-query
is_installed() {
	local -r pkg="$1"
	set +e
	dpkg-query -s $pkg > /dev/null 2>&1
	local rc=$?
	set -e
	if [ $rc = 0 ]; then
		echo 0
	else
		echo 1
	fi
}

# depends: apt
do_install() {
	local -r pkg="$1"
	apt-get install -y $pkg > /dev/null
}

# FIXME This will create problems if the interface is already configured
# using /etc/network/interfaces.
create_iface_conf() {
	local -r conf="$1"
	local -r iface="$2"
	local -r iface_addr="$3"
	local -r iface_mask="$4"
	local -r f=$conf
	> $f
	echo "allow-hotplug $iface" >> $f
	echo "iface $iface inet static" >> $f
	echo -e "\taddress\t$iface_addr" >> $f
	echo -e "\tnetmask\t$iface_mask" >> $f
}

enable_ipforwarding() {
	local -r conf="/etc/sysctl.conf"
	local -r f=$conf
	sed -i -E 's/#(net\.ipv4\.ip_forward)=.*/\1=1/' $f
	sysctl -q -p $f
}

enable_nat() {
	local -r iface_uplink="$1"
	local -r iface_downlink="$2"
	local -r conf="/etc/iptables-rules.conf"
	local -r f=$conf

	# TODO Change to use nft
	> $f
	echo "#!/bin/bash" >> $f
	echo "" >> $f
	echo "set -e" >> $f
	echo "set -u" >> $f
	echo "" >> $f
	echo "IFACE_UPLINK=$iface_uplink" >> $f
	echo "IFACE_DOWNLINK=$iface_downlink" >> $f
	echo "" >> $f
	echo "iptables-nft -t nat -A POSTROUTING -o \$IFACE_UPLINK -j MASQUERADE" >> $f
	echo "iptables-nft -A FORWARD -i \$IFACE_DOWNLINK -o \$IFACE_UPLINK -j ACCEPT" >> $f
	echo "iptables-nft -A FORWARD -i \$IFACE_UPLINK -o \$IFACE_DOWNLINK -j ACCEPT" >> $f
	chmod +x $f
	# TODO Make sure the script is executed on boot/setup is reboot-safe!
}

create_dnsmasq_conf() {
	local -r conf="$1"
	local -r iface="$2"
	local -r range_start="$3"
	local -r range_stop="$4"
	local -r tftp_root="$5"
	# BBB
	local -r vendor_str_rom_bbb="AM335x ROM"
	local -r file_after_rom_bbb="$cfg_tftp_subdir_bbb/u-boot-spl.bin"
	local -r vendor_str_spl_bbb="AM335x U-Boot SPL"
	local -r file_after_spl_bbb="$cfg_tftp_subdir_bbb/u-boot.img"
	# APU1
	local -r vendor_str_rom_apu1="PXEClient:Arch:00000:UNDI:002001"
	local -r file_after_rom_apu1="$cfg_tftp_subdir_apu1/pxelinux.0"
	# ALIX
	local -r vendor_str_rom_alix="PXEClient:Arch:00000:UNDI:002001"
	local -r file_after_rom_alix="$cfg_tftp_subdir_alix/pxelinux.0"
	# x86_64 (ThinkPad X250)
	local -r vendor_str_rom_x86_64="PXEClient:Arch:00000:UNDI:002001"
	local -r file_after_rom_x86_64="$cfg_tftp_subdir_x86_64/pxelinux.0"
	# x86_64 (Asus M5A88V)
	local -r vendor_str_rom_m5a88v=""
	local -r file_after_rom_m5a88v="$cfg_tftp_subdir_m5a88v/pxelinux.0"

	local -r f=$conf
	> $f
	echo "# bind-interfaces statement will allow this dnsmasq to" >> $f
	echo "# co-exist with other dnsmasq instances (like the one" >> $f
	echo "# used by libvirt. The drawback is that it will fail" >> $f
	echo "# to start, if the specified network interface is not" >> $f
	echo "# present." >> $f
	echo "bind-interfaces" >> $f
	echo "interface=$iface" >> $f
	echo "log-dhcp" >> $f
	echo "dhcp-range=$range_start,$range_stop" >> $f
	echo "enable-tftp=$iface" >> $f
	echo "tftp-root=$tftp_root" >> $f
	echo "bootp-dynamic" >> $f
	echo "" >> $f
	echo "# ARMv7" >> $f
	echo "dhcp-vendorclass=set:uboot_spl,$vendor_str_rom_bbb" >> $f
	echo "dhcp-boot=tag:uboot_spl,$file_after_rom_bbb" >> $f
	echo "dhcp-vendorclass=set:uboot_img,$vendor_str_spl_bbb" >> $f
	echo "dhcp-range=tag:uboot_img,$range_start,$range_stop" >> $f
	echo "dhcp-boot=tag:uboot_img,$file_after_spl_bbb" >> $f
	# APU1
	echo "" >> $f
	echo "# PXE (64-bit BIOS)" >> $f
	echo "dhcp-vendorclass=set:pxe64,$vendor_str_rom_apu1" >> $f
	echo "dhcp-boot=tag:pxe64,$file_after_rom_apu1" >> $f
	echo "dhcp-range=tag:pxe64,$range_start,$range_stop" >> $f
	# ALIX
	echo "" >> $f
	echo "# PXE (32-bit BIOS)" >> $f
	echo "dhcp-vendorclass=set:pxe32,$vendor_str_rom_alix" >> $f
	echo "dhcp-boot=tag:pxe32,$file_after_rom_alix" >> $f
	echo "dhcp-range=tag:pxe32,$range_start,$range_stop" >> $f
	# x86_64 (ThinkPad X250)
	echo "" >> $f
	echo "# x86_64 (64-bit ThinkPad X250)" >> $f
	echo "dhcp-vendorclass=set:x86_64,$vendor_str_rom_x86_64" >> $f
	echo "dhcp-boot=tag:x86_64,$file_after_rom_x86_64" >> $f
	echo "dhcp-range=tag:x86_64,$range_start,$range_stop" >> $f
	# x86_64 (M5A88V)
	echo "" >> $f
	echo "# x86_64 (64-bit Asus M5A88V)" >> $f
	echo "dhcp-vendorclass=set:x86_64-m5a88v,$vendor_str_rom_m5a88v" >> $f
	echo "dhcp-boot=tag:x86_64-m5a88v,$file_after_rom_m5a88v" >> $f
	echo "dhcp-range=tag:x86_64-m5a88v,$range_start,$range_stop" >> $f
}

create_nfs_conf() {
	local -r root_share="$1"
	local -r cidr="$2"
	local -r share_opts="rw,sync,no_subtree_check,no_root_squash"
	local -r conf_file="$root_share $cidr($share_opts)"
	local -r f="/etc/exports"
	# Check if share already exists in /etc/exports
	set +e
	grep -q "^$root_share " $f
	local rc=$?
	set -e
	if [ $rc == 0 ]; then
		# If it exists, use sed to update entry if not equal
		# But first, we need to escape the path and cidr of
		# the share to be used in a regular expression
		local root_share_escaped="$(echo $root_share | sed 's/\//\\\//g')"
		local cidr_escaped="$(echo $cidr | sed 's/\//\\\//g')"
		sed -i -E "s/^($root_share_escaped) .*$/\1 $cidr_escaped($share_opts)/" $f
	else
		echo "$root_share $cidr($share_opts)" >> $f
	fi
}

enable_nfs_v2() {
	local -r f="/etc/default/nfs-kernel-server"
	# Check if already enabled
	set +e
	grep -q '^RPCNFSDCOUNT="8 -V2"$' $f
	local rc=$?
	set -e
	if [ $rc != 0 ]; then
		sed -i -E 's/(RPCNFSDCOUNT=)(.*)/\1"8 -V2"/' $f
	fi
}

# depends: losetup
cp_from_img() {
	local -r img_file="$1"
	local -r img_ext="$(echo "$img_file" | sed -E 's/.*\.(.*)$/\1/')"
	local -r src="$2"
	local -r dst="$3"
	local -r mntpoint="tmp"
	if [[ "$img_ext" == "img" ]]; then
		local -r lo_diskdev=$(losetup --find --partscan --show $img_file)
		local -r lo_partdev="${lo_diskdev}p1"
		local -r blkdev=$lo_partdev
		mkdir -p $mntpoint
		mount $blkdev $mntpoint
		cp -rf $mntpoint/$src $dst
		umount $mntpoint
		rmdir $mntpoint
		losetup -d $lo_diskdev
	elif [[ "$img_ext" == "iso" ]]; then
		mkdir -p $mntpoint
		mount -o loop,ro $img_file $mntpoint
		cp -rf $mntpoint/$src $dst
		umount $mntpoint
		rmdir $mntpoint
	else
		err "cp_from_img(): Unsupported file extension!"
		exit 1
	fi
}

# FIXME Parts of this function are expected in debian amd64 installer
# FIXME Make function more flexible when it comes to what should to into the initrd
update_initrd() {
	local cache_dir="$1"
	local target="$2"
	local cfg_tftp_root="$3"
	local cfg_tftp_subdir="$4"

	# Fetch non-free binary firmware
	local f="firmware.cpio.gz"
	local u="http://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/$f"
	if [ ! -e $cache_dir/$target/$f ]; then
		log "Fetching non-free firmware archive ..."
		wget -q -P $cache_dir/$target $u
	fi

	# Re-generate the initrd.gz of the installer, to include the
	# missing firmware.

	# TODO Have a look at this page in order to simplify re-creation
	# of initrd.gz.
	#
	# https://wiki.debian.org/DebianInstaller/NetbootFirmware
	workdir="$PWD"
	log "Extracting initrd.gz to allow insertion of non-free firmware ..."
	pushd $cfg_tftp_root/$cfg_tftp_subdir/debian-installer/amd64 > /dev/null
	mkdir -p fake-root-initrd
	pushd fake-root-initrd > /dev/null
	zcat ../initrd.gz | cpio -id
	# Extract firmware directory from cpio file in /
	log "Extracting non-free firmware into / of initrd file system ..."
	zcat $workdir/$cache_dir/$target/firmware.cpio.gz | cpio -id
	log "Re-generating initrd with non-free firmware ..."
	find . | cpio --create --format='newc' > ../initrd
	log "Backing up original initrd.gz ..."
	mv ../initrd.gz ../initrd-original.gz
	log "Creating new initrd.gz ..."
	gzip ../initrd
	popd # fake-root-initd
	log "Cleaning up fake-root-initrd"
	rm -rf fake-root-initrd
	popd # $cfg_tftp_root/$cfg_tftp_subdir/debian-installer/amd64
}

tweak_rootfs_configure_iface() {
	local -r rootfs="$1"
	local -r iface="$2"
	local -r f=$rootfs/etc/network/interfaces
	echo "allow-hotplug $iface" >> $f
	echo "iface $iface inet dhcp" >> $f
}

# Will disable creation of usb0 and usb1 network interfaces
tweak_rootfs_disable_usb_netifaces() {
	local -r rootfs="$1"
	# do equivalent of 'systemctl mask generic-board-startup'
	local f=$rootfs/etc/systemd/system/generic-board-startup.service
	if [ -h $f ]; then
		rm $f
	fi
	ln -s /dev/null $f
	local f=$rootfs/etc/systemd/system/multi-user.target.wants/generic-board-startup.service
	if [ -h $f ]; then
		rm $f
	fi
	ln -s /dev/null $f
}

tweak_rootfs_enable_autologin() {
	local -r rootfs="$1"
	local f="$rootfs/lib/systemd/system/serial-getty@.service"
	sed -i -E 's/^(ExecStart=-\/sbin\/agetty) (.*)$/\1 --autologin root \2/' $f
}

setup_network() {
	if [[ -e $cfg_iface_conf && $opt_force == 0 ]]; then
		log "WARNING: Found existing interface configuration ($cfg_iface_conf)! Remove file manually or use -f to overwrite"
	else
		log "Creating network interface configuration ..."
		create_iface_conf $cfg_iface_conf $cfg_iface $cfg_iface_addr $cfg_iface_mask
		# Before attempting to bring the interface up,
		# we check if it exists/is connected.
		# NOTE When usbN is used, it is common that the
		# interface is not visible all of the time.
		if [ -e /sys/class/net/$cfg_iface ]; then
			# TODO Check if we must bring the interface down, in
			# order to have the configuration for this interface
			# reloaded.
			# Restart network interface or run ifup $iface
			ifup $cfg_iface
		fi
	fi

	#
	# IP Forwarding Setup
	#

	log "Setting up IP forwarding ..."
	enable_ipforwarding
	log "Setting up NAT using uplink ..."
	enable_nat $cfg_iface_uplink $cfg_iface
}

setup_http() {
	local -r pkg="lighttpd"
	if [ "$(is_installed "$pkg")" = 1 ]; then
		log "Installing HTTP server ..."
		do_install $pkg
	fi
	# Enable access log for debugging
	set +e
	lighttpd-enable-mod accesslog
	set -e
	systemctl restart lighttpd
	local -r preseed_repo_dir="$cfg_preseed_repo_dir"
	local -r http_docroot="$cfg_http_docroot"
	local -r http_preseed_prefix="$cfg_http_preseed_prefix"
	local -r http_preseed_dir="$cfg_http_preseed_dir"
	if [ -L $http_preseed_dir ]; then
		rm $http_preseed_dir
	fi
	log "Making preseed files available via HTTP"
	ln -s $preseed_repo_dir $http_preseed_dir
}

setup_nfs() {
	#
	# Dependency Check
	#

	local -r deps="wget xz-utils"
	for pkg in $deps; do
		if [ "$(is_installed "$pkg")" = 1 ]; then
			do_install $pkg
		fi
	done

	log "Checking NFS server is installed ..."
	local pkg="nfs-kernel-server"
	if [ "$(is_installed "$pkg")" = 1 ]; then
		log "installing NFS server ..."
		do_install $pkg
	fi
	log "Enabling NFS version 2 ..."
	enable_nfs_v2
	# for each target, create share, fetch the system image
	for target in $targets; do
		local var_name="cfg_nfs_root_share_$target"
		local cfg_nfs_root_share="${!var_name}"
		log "Creating NFS configuration ..."
		create_nfs_conf $cfg_nfs_root_share "$cfg_net_addr_cidr"
		if [ ! -e $cfg_nfs_root_share ]; then
			log "Creating NFS root file system share ..."
			mkdir -p $cfg_nfs_root_share
		fi
		local var_name="cfg_img_archive_$target"
		local cfg_img_archive="${!var_name}"
		local var_name="cfg_img_url_$target"
		local cfg_img_url="${!var_name}"
		if [ ! -e $cache_dir/$target/$cfg_img_archive ]; then
			log "Fetching system image ..."
			wget -q -P $cache_dir/$target $cfg_img_url
		fi
		local var_name="cfg_img_file_$target"
		local cfg_img_file="${!var_name}"
		if [[ "$target" == "bbb" ]]; then
			if [ ! -e $cache_dir/$target/$cfg_img_file ]; then
				log "Uncompressing system image ..."
				xzcat $cache_dir/$target/$cfg_img_archive > $cache_dir/$target/$cfg_img_file
			fi
		fi
		log "Populating NFS root file system share ..."
		local var_name="cfg_nfs_root_share_$target"
		local cfg_nfs_root_share="${!var_name}"
		if [[ "$target" == "bbb" ]]; then
			cp_from_img $cache_dir/$target/$cfg_img_file "*" $cfg_nfs_root_share
		fi
		if [[ "$target" == "bbb" ]]; then
			# Clear existing interface configuration
			> $cfg_nfs_root_share/etc/network/interfaces
			tweak_rootfs_configure_iface $cfg_nfs_root_share "eth0"
			tweak_rootfs_disable_usb_netifaces $cfg_nfs_root_share
			tweak_rootfs_enable_autologin $cfg_nfs_root_share
		fi
	done
	set +e
	systemctl restart nfs-kernel-server
	set -e
}

setup_tftp() {
	if [ ! -e $cfg_tftp_root ]; then
		log "creating TFTP directory ..."
		mkdir -p $cfg_tftp_root
	fi
	local var_name=""
	local cfg_img_archive=""
	local cfg_tftp_subdir=""
	local cfg_nfs_root_share=""
	local kernel_version=""
	local kernel=""
	local fdtfile=""
	local d=""
	local f=""
	for target in $targets; do
		log "Populating TFTP directory ..."
		var_name="cfg_tftp_subdir_$target"
		cfg_tftp_subdir="${!var_name}"
		# Shortcut for target specific TFTP subdirectory
		d="$cfg_tftp_root/$cfg_tftp_subdir"
		# Create target specific subdirectory
		mkdir -p $d

		var_name="cfg_nfs_root_share_$target"
		cfg_nfs_root_share="${!var_name}"
		if [[ "$target" == "bbb" ]]; then
			# Deploy previously built U-Boot binaries
			cp $cache_dir/$target/u-boot-spl.bin $d
			cp $cache_dir/$target/u-boot.img $d
			chown nobody:nogroup $d/u-boot-spl.bin
			chown nobody:nogroup $d/u-boot.img
			# Deploy Kernel and DTB
			kernel_version="$(ls -1 $cfg_nfs_root_share/lib/modules | tail -n 1)"
			kernel="vmlinuz-$kernel_version"
			fdtfile="am335x-boneblack.dtb"
			cp $cfg_nfs_root_share/boot/dtbs/$kernel_version/$fdtfile $d
			cp $cfg_nfs_root_share/boot/$kernel $d
			# Create symlink for kernel
			f=$d/zImage
			if [ -e $f ]; then
				rm $f
			fi
			ln -r -s $d/$kernel $f
		elif [[ "$target" == "alix" ]]; then
			var_name="cfg_img_archive_$target"
			cfg_img_archive="${!var_name}"
			f="$cache_dir/$target/$cfg_img_archive"
			tar xzf $f -C $d
			f="$d/debian-installer/i386/boot-screens/syslinux.cfg"
			echo "SERIAL 0 115200" >> $f
			for f in $(grep -r "vga=788" $d | cut -d':' -f1 | uniq); do
				sed -i 's/vga=788/console=ttyS0,115200n8 net.ifnames=0/' $f
			done
		elif [[ "$target" == "apu1" ]]; then
			var_name="cfg_img_archive_$target"
			cfg_img_archive="${!var_name}"
			f="$cache_dir/$target/$cfg_img_archive"
			tar xzf $f -C $d
			f="$d/debian-installer/amd64/boot-screens/syslinux.cfg"
			echo "SERIAL 0 115200" >> $f
			for f in $(grep -r "vga=788" $d | cut -d':' -f1 | uniq); do
				sed -i 's/vga=788/console=ttyS0,115200n8 net.ifnames=0/' $f
			done
			# NOTE For APU1D4 we need to provide extra firmware binaries
			# for the Realtek network interface to work.
			update_initrd "$cache_dir" "$target" "$cfg_tftp_root" "$cfg_tftp_subdir"
		elif [[ "$target" == "x86_64" ]]; then
			var_name="cfg_img_archive_$target"
			cfg_img_archive="${!var_name}"
			f="$cache_dir/$target/$cfg_img_archive"
			tar xzf $f -C $d
			f="$d/debian-installer/amd64/boot-screens/syslinux.cfg"
			for f in $(grep -r "vga=788" $d | cut -d':' -f1 | uniq); do
				sed -i 's/vga=788/vga=788 net.ifnames=0/' $f
			done
		elif [[ "$target" == "m5a88v" ]]; then
			var_name="cfg_img_archive_$target"
			cfg_img_archive="${!var_name}"
			f="$cache_dir/$target/$cfg_img_archive"
			tar xzf $f -C $d
			f="$d/debian-installer/amd64/boot-screens/syslinux.cfg"
			for f in $(grep -r "vga=788" $d | cut -d':' -f1 | uniq); do
				sed -i 's/vga=788/vga=788 net.ifnames=0/' $f
			done
			# NOTE For M5A88V we need to provide extra firmware binaries
			# for the Realteknetwork interface to work.
			update_initrd "$cache_dir" "$target" "$cfg_tftp_root" "$cfg_tftp_subdir"
		else
			err "Can't populate TFTP directory! Unsupported target!"
			exit 1
		fi
	done
}

setup_dnsmasq() {
	log "Checking DNSMASQ is installed ..."
	local pkg="dnsmasq"
	if [ "$(is_installed "$pkg")" = 1 ]; then
		log "Installing DNSMASQ ..."
		do_install $pkg
	fi
	if [[ -e $cfg_dnsmasq_conf && $opt_force == 0 ]]; then
		log "WARNING: Found existing dnsmasq configuration ($cfg_dnsmasq_conf)! Remove file manually or use -f to overwrite"
	else
		log "Creating DNSMASQ config ..."
		create_dnsmasq_conf $cfg_dnsmasq_conf $cfg_iface $cfg_range_start $cfg_range_stop $cfg_tftp_root
		systemctl restart dnsmasq
	fi
}

usage() {
	echo "USAGE: $(basename $0) <options> <target>..."
	echo ""
	echo "OPTIONS"
	echo ""
	echo "  -d    display configuration"
	echo "  -f    overwrite config files if already exist"
	echo "  -h    show this dialog"
	echo "  -q    quiet operation"
	echo "  -v    show version"
	echo ""
	echo "TARGETS"
	echo ""
	echo "  bbb   BeagleBone Black"
	echo "  alix  PC Engines ALIX"
	echo "  apu1  PC Engines APU1"
	echo "  x86_64	regular 64-bit x86 PC (ThinkPad X250)"
	echo "  m5a88v  regular 64-bit x86 PC (with RTL8111 Ethernet / requires binary firmware in initrd)"
	echo""
}

# depends: sed, systemctl, wget, xzcat, losetup
main() {
	# Set default values for options
	opt_dump_cfg=0
	opt_force=0
	opt_quiet=0

	source $(dirname $0)/libcidr
	source $(dirname $0)/config.sh

	local -r cache_dir="cache"

	# Parse options
	while getopts "dfhqv" opt; do
		case $opt in
		"d")
			opt_dump_cfg=1
			;;
		"f")
			opt_force=1
			;;
		"h")
			usage
			exit 0
			;;
		"q")
			opt_quiet=1
			;;
		"v")
			echo $VERSION
			exit 0
			;;
		*)
			err "Unknown option!"
			exit 1
			;;
		esac
	done
	shift $((OPTIND - 1))

	# Parse remaining arguments

	# Targets to prepare network booting for
	local -r supported_targets="bbb alix apu1 x86_64 m5a88v"
	local -r default_targets="bbb"
	local targets=""
	if [[ $# > 0 ]]; then
		for arg in $*; do
			# if not suported
			if [ $(is_in "$arg" "$supported_targets") == 1 ]; then
				err "Unsupported target $arg!"
				exit 1
			fi
			targets="$targets $arg"
		done
	else
		targets="$default_targets"
	fi


	# Dump configuration and exit
	if [ $opt_dump_cfg = 1 ]; then
		echo "# COMMON"
		printf "%-20s %s\n" "iface:" "$cfg_iface"
		printf "%-20s %s\n" "iface_addr:" "$cfg_iface_addr"
		printf "%-20s %s\n" "iface_mask:" "$cfg_iface_mask"
		printf "%-20s %s\n" "iface_uplink:" "$cfg_iface_uplink"
		printf "%-20s %s\n" "net_addr_cidr:" "$cfg_net_addr_cidr"
		printf "%-20s %s\n" "interfaces_confdir:" "$cfg_interfaces_confdir"
		printf "%-20s %s\n" "iface_conf:" "$cfg_iface_conf"
		printf "%-20s %s\n" "dnsmasq_confdir:" "$cfg_dnsmasq_confdir"
		printf "%-20s %s\n" "dnsmasq_conf:" "$cfg_dnsmasq_conf"
		printf "%-20s %s\n" "range_start:" "$cfg_range_start"
		printf "%-20s %s\n" "range_stop:" "$cfg_range_stop"
		printf "%-20s %s\n" "tftp_root:" "$cfg_tftp_root"

		# Only when preseed support is configured
		printf "%-20s %s\n" "preseed_repo_dir:" "$cfg_preseed_repo_dir"
		printf "%-20s %s\n" "http_docroot:" "$cfg_http_docroot"
		printf "%-20s %s\n" "http_preseed_prefix:" "$cfg_http_preseed_prefix"
		printf "%-20s %s\n" "http_preseed_dir:" "$cfg_http_preseed_dir"

		for target in $targets; do
			var_name="cfg_img_archive_$target"
			cfg_img_archive="${!var_name}"
			var_name="cfg_img_url_$target"
			cfg_img_url="${!var_name}"
			var_name="cfg_img_file_$target"
			cfg_img_file="${!var_name}"
			var_name="cfg_tftp_subdir_$target"
			cfg_tftp_subdir="${!var_name}"
			var_name="cfg_nfs_root_share_$target"
			cfg_nfs_root_share="${!var_name}"
			echo "# Target: ${target^^}"
			printf "%-20s %s\n" "img_archive:" "$cfg_img_archive"
			printf "%-20s %s\n" "img_url:" "$cfg_img_url"
			printf "%-20s %s\n" "img_file:" "$cfg_img_file"
			printf "%-20s %s\n" "tftp_subdir:" "$cfg_tftp_subdir"
			printf "%-20s %s\n" "nfs_root_share:" "$cfg_nfs_root_share"
		done
		exit 0
	fi

	log "Selected targets: $targets"

	if [ $UID != 0 ]; then
		err "Run as root!"
		exit 1
	fi

	if [ $(is_in "bbb" "$targets") == 0 ]; then
		#
		# Build U-Boot
		#

		# TODO Check if git is installed
		# TODO Check make is installed

		local pkg="gcc-arm-linux-gnueabi"
		if [ "$(is_installed "$pkg")" = 1 ]; then
			log "Installing cross compiler ..."
			do_install $pkg
		fi
		base_dir="$PWD"
		# Change into cache directory
		mkdir -p $cache_dir/bbb
		pushd $cache_dir/bbb > /dev/null
		if [ ! -e u-boot ]; then
			log "Fetching U-Boot sources ..."
			# TODO Set ssh options to accept key without confirmation
			git clone --depth 1 git://git.denx.de/u-boot.git > /dev/null 2>&1
		fi
		MAKEFLAGS=""
		MAKEFLAGS="$MAKEFLAGS -j$(nproc)"
		MAKEFLAGS="$MAKEFLAGS ARCH=arm CROSS_COMPILE=arm-linux-gnueabi-"
		# TODO Check ccache is available!
		MAKEFLAGS="$MAKEFLAGS CC=/usr/lib/ccache/arm-linux-gnueabi-gcc"
		MAKEFLAGS="$MAKEFLAGS HOSTCC=/usr/lib/ccache/gcc"
		pushd u-boot > /dev/null
		log "Patching U-Boot"
		# Customize U-Boot environment using patch.
		# TODO Find a better way (uEnv.txt if it is possible to send via TFTP).
		patch -s -p1 --batch < $base_dir/patches/bbb/u-boot/0001-Customize-U-Boot-default-environment.patch > /dev/null
		log "Configuring U-Boot"
		make $MAKEFLAGS am335x_evm_defconfig > /dev/null
		log "Building U-Boot ..."
		make $MAKEFLAGS > /dev/null
		# Copy artifacts to cache directory. Will be picked up by
		# TFTP root directory population steps.
		cp spl/u-boot-spl.bin $base_dir/$cache_dir/bbb
		cp u-boot.img $base_dir/$cache_dir/bbb
		popd > /dev/null
		popd > /dev/null
	fi

	setup_network
	setup_http
	setup_nfs
	setup_tftp
	setup_dnsmasq

	log "Finished!"
}

main $@
