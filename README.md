# Network Boot for BeagleBone Black

Simplified setup for host system to allow network booting BeagleBone Black devices.

NOTE Use RJ-45 connector or BeagleBone Black, not the miniUSB
for network communication. Generally, it is possible to
use USB for network booting. But this setup is not configured
for this scenario. It might be in the future.

## Setup

```
sudo ./setup.sh
```

## Usage

 * computer to act as boot host
 * network cable
 * BeagleBone Black
 * 5V power adapter (or miniUSB cable)
 * USB serial adapter
 * system image on microSD card (only used for U-Boot)

### Preparations

In order to avoid confusion, we need to erase the U-Boot from the internal eMMC.
This way we can be sure, only the bootloader from the microSD card is used.

 1. connect USB serial adapter to BBB J1 header and PC
 2. power up BBB without microSD card
 3. interrupt U-Boot with space key
 4. erase U-Boot from eMMC

These are the commands to be used in order to erase U-Boot from eMMC.

```
mmc dev 1
mmc erase 0 512
```


### Adjust Boot Sequence

The default boot sequence by of the BeagleBone black does not include
the Ethernet interface. In order to allow fetching U-Boot via Ethernet
(not USB Ethernet gedget), we have to modify the boot sequence
configuration by adding jumper cables to the ```DSS_Dn``` pins
which also act as ```SYSBOOT``` pins.

Default boot sequence (SYSBOOT[4:0]: ```11100b```)

```
MMC1, MMC0, UART0, USB0
```

Change to followoing sequence (SYSBOOT[4:0]: ```01000b```) by
pulling ```SYSBOOT4``` (aka ```DSS_D4```) and ```SYSBOOT2``` (aka ```DSS_D2```) low (to ```DGND```).

```
EMAC1, MMC0, XIP, NANDI2C
```

### U-Boot Configuration

During the execution of setup.sh, the default U-Boot environment will be customized
to the following equivalent settings. Note that there is no need to manually
interrupt U-Boot to provide these. This listing is just for reference.

```
setenv bootcmd run netboot
setenv ethact cpsw
setenv fdtfile am335x-boneblack.dtb
setenv serverip <use-iface-addr-from-config>
setenv rootpath /srv/nfs/bbb-root
```

> NOTE: Setting serverip might seem redudant as DHCP is used. Still
> it is required for proper operation. Otherwise the NFS root file
> system will not be mounted by the kernel, resulting in a kernel panic.

### Manual Steps

Inside the BBB, run the following command to install the ```nfs-common```
package. This is needed to mount other NFS shares.

```
apt update
apt install -y nfs-common
```

## TODO

 * try to force AM335 ROM (via SYSBOOT pins) to use eth0 for fetching SPL and U-Boot
   * document how to change SYSBOOT pins
 * build and deploy kernel with g_ether built-in
 * make sure no other network interface has same address (otherwise sending TFTP files will fail)
 * allow full operation with regular user (without having file
   permission issues most likely caused by wrong NFS config or
   file permissions when copying the root fs from the image file)
 * automatically apply U-Boot patch
 * enable SSH root login without password
 * check if usb network interfaces still work
 * check if usb serial is working
