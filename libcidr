#!/bin/bash

n_bits_set() {
	local -r bits_in_byte=8
	local v="$1"
	local bits=0
	for i in $(seq $bits_in_byte); do
		# right-most bit is set?
		bit_set=$((v % 2))
		# add to number of bits
		bits=$((bits + bit_set))
		# right-shift byte to inspect next bit
		v=$((v >> 1))
	done
	echo $bits
}

n_bits_set_in_netmask() {
	local -r bytes_in_ipv4=4
	local -r mask="$1"
	local bits=0
	# Determine the number of bits of the mask
	for i in $(seq $bytes_in_ipv4); do
		# Get byte of mask
		v=$(echo $mask | cut -d. -f$i)
		# Abort if 0
		if [ $v = 0 ]; then
			break;
		fi
		bits_set=$(n_bits_set $v)
		bits=$((bits + bits_set))
	done
	echo $bits
}

to_cidr() {
	local -r ipv4_bits=32
	local -r addr="$1"
	local -r mask="$2"
	local -r net_bits=$(n_bits_set_in_netmask $mask)
	local -r host_bits=$((ipv4_bits - net_bits))
	# Calculate the number of bytes that we need
	# to modify in order to get a network address
	na_bytes=$((net_bits / 8))
	cidr=""
	for j in $(seq 4); do
		if [ $j != 1 ]; then
			cidr="$cidr."
		fi
		v=$(echo $addr | cut -d. -f$j)
		if (( $j <= $na_bytes )); then
			cidr="${cidr}${v}"
		else
			cidr="${cidr}0"
		fi
	done
	echo "$cidr/$net_bits"
}
