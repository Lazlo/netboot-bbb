# Network Interface Configuration
cfg_iface="eth0"
cfg_iface_addr="15.0.0.1"
cfg_iface_mask="255.255.255.0"
cfg_iface_uplink="eth1"
cfg_net_addr_cidr="$(to_cidr $cfg_iface_addr $cfg_iface_mask)"
cfg_interfaces_confdir="/etc/network/interfaces.d"
cfg_iface_conf="$cfg_interfaces_confdir/$cfg_iface"

# DNSMASQ Configuration
cfg_dnsmasq_confdir="/etc/dnsmasq.d"
cfg_dnsmasq_conf="$cfg_dnsmasq_confdir/$cfg_iface-netboot.conf"
cfg_range_start="15.0.0.50"
cfg_range_stop="15.0.0.60"
cfg_tftp_root="/srv/tftp"

# HTTP / Preseed Configuration
cfg_preseed_repo_dir="/home/lazlo/Workspace/lazlo/infra/preseed"
cfg_http_docroot="/var/www/html"
cfg_http_preseed_prefix="preseed"
cfg_http_preseed_dir="$cfg_http_docroot/$cfg_http_preseed_prefix"

#
# BeagleBone Black
#

# System Image Configuration
cfg_img_archive_bbb="bone-debian-9.5-iot-armhf-2018-10-07-4gb.img.xz"
cfg_img_url_bbb="https://debian.beagleboard.org/images/$cfg_img_archive_bbb"
cfg_img_file_bbb="$(echo $cfg_img_archive_bbb | sed 's/\.xz$//')"
# TFTP Configuration
cfg_tftp_subdir_bbb="boot/bbb"
# NFS Configuration
cfg_nfs_root_share_bbb="/srv/nfs/bbb-root"

#
# PC Engines ALIX
#

# System Image Configuration
cfg_img_archive_alix="netboot.tar.gz"
cfg_img_url_alix="http://ftp.nl.debian.org/debian/dists/buster/main/installer-i386/current/images/netboot/$cfg_img_archive_alix"
cfg_img_file_alix="$cfg_img_archive_alix"
# TFTP Configuration
cfg_tftp_subdir_alix="install/alix"
# NFS Configuration
cfg_nfs_root_share_alix="/srv/nfs/alix-root"

#
# PC Engines APU1
#

# System Image Configuration
cfg_img_archive_apu1="netboot.tar.gz"
cfg_img_url_apu1="http://ftp.nl.debian.org/debian/dists/buster/main/installer-amd64/current/images/netboot/$cfg_img_archive_apu1"
cfg_img_file_apu1="$cfg_img_archive_apu1"
# TFTP Configuration
cfg_tftp_subdir_apu1="install/apu1"
# NFS Configuration
cfg_nfs_root_share_apu1="/srv/nfs/apu1-root"

#
# x86_64 (ThinkPad X250)
#

cfg_img_archive_x86_64="netboot.tar.gz"
cfg_img_url_x86_64="http://ftp.nl.debian.org/debian/dists/buster/main/installer-amd64/current/images/netboot/$cfg_img_archive_x86_64"
cfg_img_file_x86_64="$cfg_img_archive_x86_64"
# TFTP Configuration
cfg_tftp_subdir_x86_64="install/x86_64"
# NFS Configuration
cfg_nfs_root_share_x86_64="/srv/nfs/x86_64-root"

#
# x86_64 (Asus M5A88V)
#

cfg_img_archive_m5a88v="netboot.tar.gz"
cfg_img_url_m5a88v="http://ftp.nl.debian.org/debian/dists/buster/main/installer-amd64/current/images/netboot/$cfg_img_archive_m5a88v"
cfg_img_file_m5a88v="$cfg_img_archive_m5a88v"
# TFTP Configuration
cfg_tftp_subdir_m5a88v="install/m5a88v"
# NFS Configuration
cfg_nfs_root_share_m5a88v="/srv/nfs/m5a88v"
